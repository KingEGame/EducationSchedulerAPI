package kg.education.scheduler.api.service;

import kg.education.scheduler.api.model.Course;
import kg.education.scheduler.api.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class CourseService {
    private final CourseRepository courseRepository;

    @Autowired
    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public List<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    // Other course-related methods...

    public Course createCourse(Course course) {
        // Logic to create a new course
        return courseRepository.save(course);
    }

    public Course updateCourse(Long id, Course course) {
        // Logic to update course with given id
        Course existingCourse = courseRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Course not found with id: " + id));

        existingCourse.setName(course.getName());

        return courseRepository.save(existingCourse);
    }

    public void deleteCourse(Long id) {
        // Logic to delete course with given id
        courseRepository.deleteById(id);
    }

    public Course getCourseById(Long id) {
        return courseRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Course not found with id: " + id));
    }
}

