package kg.education.scheduler.api.service;

import kg.education.scheduler.api.model.Assignment;
import kg.education.scheduler.api.repository.AssignmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class AssignmentService {
    private final AssignmentRepository assignmentRepository;

    @Autowired
    public AssignmentService(AssignmentRepository assignmentRepository) {
        this.assignmentRepository = assignmentRepository;
    }

    public List<Assignment> getCourseAssignments(Long courseId) {
        return assignmentRepository.findByCourseIdOrderByDeadlineAsc(courseId);
    }

    // Other assignment-related methods...

    public Assignment createAssignment(Assignment assignment) {
        // Logic to create a new assignment
        return assignmentRepository.save(assignment);
    }

    public Assignment updateAssignment(Long id, Assignment assignment) {
        // Logic to update assignment with given id
        Assignment existingAssignment = assignmentRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Assignment not found with id: " + id));

        existingAssignment.setName(assignment.getName());
        existingAssignment.setDeadline(assignment.getDeadline());

        return assignmentRepository.save(existingAssignment);
    }

    public void deleteAssignment(Long id) {
        // Logic to delete assignment with given id
        assignmentRepository.deleteById(id);
    }

    public Assignment getAssignmentById(Long id) {
        return assignmentRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Assignment not found with id: " + id));
    }
}

