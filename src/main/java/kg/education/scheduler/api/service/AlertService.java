package kg.education.scheduler.api.service;

import kg.education.scheduler.api.model.Alert;
import kg.education.scheduler.api.repository.AlertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class AlertService {
    private final AlertRepository alertRepository;

    @Autowired
    public AlertService(AlertRepository alertRepository) {
        this.alertRepository = alertRepository;
    }

    public List<Alert> getUserAlerts(Long userId) {
        return alertRepository.findByUserIdOrderByTimestampDesc(userId);
    }

    // Other alert-related methods...

    public Alert createAlert(Alert alert) {
        // Logic to create a new alert
        return alertRepository.save(alert);
    }

    public Alert updateAlert(Long id, Alert alert) {
        // Logic to update alert with given id
        Alert existingAlert = alertRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Alert not found with id: " + id));

        existingAlert.setMessage(alert.getMessage());

        return alertRepository.save(existingAlert);
    }

    public void deleteAlert(Long id) {
        // Logic to delete alert with given id
        alertRepository.deleteById(id);
    }

    public Alert getAlertById(Long id) {
        return alertRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Alert not found with id: " + id));
    }
}

