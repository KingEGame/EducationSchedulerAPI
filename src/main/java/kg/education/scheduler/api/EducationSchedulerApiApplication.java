package kg.education.scheduler.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EducationSchedulerApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(EducationSchedulerApiApplication.class, args);
    }

}
