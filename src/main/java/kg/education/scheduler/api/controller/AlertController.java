package kg.education.scheduler.api.controller;

import kg.education.scheduler.api.model.Alert;
import kg.education.scheduler.api.service.AlertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/alerts")
public class AlertController {
    private final AlertService alertService;

    @Autowired
    public AlertController(AlertService alertService) {
        this.alertService = alertService;
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<List<Alert>> getUserAlerts(@PathVariable Long userId) {
        List<Alert> alerts = alertService.getUserAlerts(userId);
        return ResponseEntity.ok(alerts);
    }

    // Other alert-related endpoints...

    @PostMapping
    public ResponseEntity<Alert> createAlert(@RequestBody Alert alert) {
        // Logic to create a new alert
        Alert createdAlert = alertService.createAlert(alert);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdAlert);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Alert> updateAlert(@PathVariable Long id, @RequestBody Alert alert) {
        // Logic to update alert with given id
        Alert updatedAlert = alertService.updateAlert(id, alert);
        return ResponseEntity.ok(updatedAlert);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAlert(@PathVariable Long id) {
        // Logic to delete alert with given id
        alertService.deleteAlert(id);
        return ResponseEntity.noContent().build();
    }
}
