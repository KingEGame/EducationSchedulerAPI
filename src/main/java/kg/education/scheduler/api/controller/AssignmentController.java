package kg.education.scheduler.api.controller;

import kg.education.scheduler.api.model.Assignment;
import kg.education.scheduler.api.service.AssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/assignments")
public class AssignmentController {
    private final AssignmentService assignmentService;

    @Autowired
    public AssignmentController(AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }

    @GetMapping("/course/{courseId}")
    public ResponseEntity<List<Assignment>> getCourseAssignments(@PathVariable Long courseId) {
        List<Assignment> assignments = assignmentService.getCourseAssignments(courseId);
        return ResponseEntity.ok(assignments);
    }

    // Other assignment-related endpoints...

    @PostMapping
    public ResponseEntity<Assignment> createAssignment(@RequestBody Assignment assignment) {
        // Logic to create a new assignment
        Assignment createdAssignment = assignmentService.createAssignment(assignment);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdAssignment);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Assignment> updateAssignment(@PathVariable Long id, @RequestBody Assignment assignment) {
        // Logic to update assignment with given id
        Assignment updatedAssignment = assignmentService.updateAssignment(id, assignment);
        return ResponseEntity.ok(updatedAssignment);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAssignment(@PathVariable Long id) {
        // Logic to delete assignment with given id
        assignmentService.deleteAssignment(id);
        return ResponseEntity.noContent().build();
    }
}

