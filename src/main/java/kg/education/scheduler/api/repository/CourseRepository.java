package kg.education.scheduler.api.repository;

import kg.education.scheduler.api.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {
    // Additional queries if needed
}

