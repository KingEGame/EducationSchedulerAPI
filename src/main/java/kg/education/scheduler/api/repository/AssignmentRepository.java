package kg.education.scheduler.api.repository;

import kg.education.scheduler.api.model.Assignment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AssignmentRepository extends JpaRepository<Assignment, Long> {
    List<Assignment> findByCourseIdOrderByDeadlineAsc(Long courseId);
}

